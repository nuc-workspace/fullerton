import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GsloginComponent } from './gslogin.component';

describe('GsloginComponent', () => {
  let component: GsloginComponent;
  let fixture: ComponentFixture<GsloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GsloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GsloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
