import { UserValidation } from './UserValidation';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gslogin',
  templateUrl: './gslogin.component.html',
  styleUrls: ['./gslogin.component.css']
})
export class GsloginComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  activateSignup:boolean=false;

  signup()
  {
    //this.router.navigate(['gslogin/signup']);
    this.activateSignup=true;
  }

  get mobile()
  {
    return this.gsLogin.controls['mobile'];
  }

  get otp()
  {
    return this.gsLogin.controls['otp'];
  }

  gsLogin=new FormGroup(
    {
      mobile:new FormControl('',[Validators.required,Validators.pattern('^[0-9]{10}'),UserValidation.noSpace]),
      otp:new FormControl('',[Validators.required,Validators.pattern('^[0-9]{6}')]),
    }
  );

  sendOtp()
  {
    this.gsLogin.controls['mobile'].disable();
  }

  verifyOtp()
  {
    if(this.otp.valid)
    {
      this.router.navigate(['gslogin/signup']);
    }
    else
    {
      console.log('Invalid OTP!');
    }
  }

}
