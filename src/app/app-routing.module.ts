import { LandingPageComponent } from './landing-page/landing-page.component';
import { SignupComponent } from './gslogin/signup/signup.component';
import { GsloginComponent } from './gslogin/gslogin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'gslogin',component:GsloginComponent,
    children:[
      {
        path:'signup',component:SignupComponent
      }
    ]
  },
  {
  path:'',component:LandingPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
